const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
//const config = require('./config/config')

const app = express()
app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors())

app.get('/', (req, res) => {
  res.send(
    [{
      title: 'Hello world',
      description: 'Hi there! How are you?'
    }]
  )
})

app.listen(process.env.PORT || 8081)
