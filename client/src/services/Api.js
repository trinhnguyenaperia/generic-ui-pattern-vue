import Axios from 'axios'

export default() => {
  return Axios.create({
    baseURL: `https://localhost:8081`
  })
}
